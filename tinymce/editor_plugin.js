

(function() {
	tinymce.create('tinymce.plugins.HiddenBlockPlugin', {
		init : function(ed, url) {
			var t = this;
			t.editor = ed;
			
			// Register commands
			ed.addCommand('mceHiddenBlock', function() {
				
				// This code is modeled on the code for mceBlockQuote
				
				var s = ed.selection, dom = ed.dom, sb, eb, n, bm, hb, r, hb2, i, nl;
				
				function isHB(n) {
					return  n.nodeName === 'DIV' && n.className === 'mcehiddenblock';
				};
				
				function getHB(e) {
					return dom.getParent(e, isHB);
				};

				// Get start/end block
				sb = dom.getParent(s.getStart(), dom.isBlock);
				eb = dom.getParent(s.getEnd(), dom.isBlock);
				
				// Remove hiddenblocks(s)
				if (hb = getHB(sb)) {	
					dom.remove(hb, 1);
					return;
				}
				if (!sb || !eb)
					return;

				// If empty paragraph node then do not use bookmark
				if (sb != eb || sb.childNodes.length > 1 || (sb.childNodes.length == 1 && sb.firstChild.nodeName != 'BR'))
					bm = s.getBookmark();
				
				// For li, the hd should go inside the li
				if (sb == eb && sb.nodeName == 'LI') {
					hb = dom.create('div', {'class': 'mcehiddenblock'});
					n = sb.firstChild;
					while (n) {
						hb.appendChild(n.cloneNode(true));
						next = n.nextSibling;
						dom.remove(n);
						n = next;
					}
					sb.appendChild(hb);
				} else {
					// make sure the hb isn't between a ul and li
					if (sb.nodeName == 'LI')
						sb=sb.parentNode;
					if (eb.nodeName == 'LI')
						eb=eb.parentNode;					
				
					// Move selected block elements into a hb
					tinymce.each(s.getSelectedBlocks(getHB(s.getStart()) || sb, getHB(s.getEnd()) || eb), function(e) {
						// Found existing HB add to this one
						if (isHB(e) && !hb) {
							hb = e;
							return;
						}
	
						// No HB found, create one
						if (!hb) {
							hb = dom.create('div', {'class': 'mcehiddenblock'});
							e.parentNode.insertBefore(hb, e);
						}
	
						// Add children from existing HB
						if (isHB(e) && hb) {
							n = e.firstChild;
	
							while (n) {
								hb.appendChild(n.cloneNode(true));
								n = n.nextSibling;
							}
	
							dom.remove(e);
							return;
						}
	
						// Add non HB element to HB
						hb.appendChild(dom.remove(e));
					});
				}
				dom.remove(dom.select('div.mcehiddenblock', hb), 1);  // remove nested hb

				if (!bm) {
					// Move caret inside empty block element
					if (!tinymce.isIE) {
						r = ed.getDoc().createRange();
						r.setStart(sb, 0);
						r.setEnd(sb, 0);
						s.setRng(r);
					} else {
						s.select(sb);
						s.collapse(1);
					}
				} else
					s.moveToBookmark(bm);
				
			});
			
			ed.onInit.add(function() {
				if (ed.settings.content_css !== false)
					ed.dom.loadCSS(url + '/css/content.css');
			});


			// Register buttons
			ed.addButton('hiddenblock', {title : 'Create hidden block', cmd : 'mceHiddenBlock', image : url + '/img/hiddenblock.gif'});
			
			// Register the name for the path: bar
            if (ed.theme.onResolveName) {
                    ed.theme.onResolveName.add(function(th, o) {
                            if (o.node.nodeName == 'DIV' && ed.dom.hasClass(o.node, 'mcehiddenblock'))
                                    o.name = 'hiddenblock';
                    });
            }
            // Add a node change handler, selects the button in the UI when a hiddenblock is selected
            ed.onNodeChange.add(function(ed, cm, n) {
            	res = !!ed.dom.getParent(n, 
        				function(n) {return n.nodeName === 'DIV' && ed.dom.hasClass(n, 'mcehiddenblock');});
            	cm.setActive('hiddenblock', !!ed.dom.getParent(n, 
            				function(n) {return n.nodeName === 'DIV' && ed.dom.hasClass(n, 'mcehiddenblock');}));
         
            });
            
            ed.onBeforeSetContent.add(function(ed, o) {
				o.content = o.content.replace(/<div([^>]+)class="hiddenblock"/g, "<div$1 class=\"mcehiddenblock\"");
			});

			ed.onPostProcess.add(function(ed, o) {
				if (o.get)
					o.content = o.content.replace(/<div([^>]+)class="mcehiddenblock"/g, "<div$1 class=\"hiddenblock\"");
			});

		},

		getInfo : function() {
			return {
				longname : 'Hidden Block Plugin',
				author : 'Matt Petro',
				authorurl : '',
				infourl : '',
				version : '1.0'
			};
		}
		
		
	});

	// Register plugin
	tinymce.PluginManager.add('hiddenblock', tinymce.plugins.HiddenBlockPlugin);
})();